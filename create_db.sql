drop table if exists users;

CREATE TABLE users(
    userID int primary key AUTO_INCREMENT,
    username varchar(255) not null,
    name varchar(50) not null,
    email varchar(255) not null,
    password varchar(255) not null,
    profileImage varchar(255) default 'assets/images/user.png',
    sessionID varchar(255) not null,
    connectionID int not null
);

INSERT INTO users(username, name, email, password, sessionID, connectionID) VALUES ('alejo123','alejandro','alejandro@gmail.com','$1$ctpSv3hb$n5f2VUkLiDw3p2jdY1iI8/','0',0);
INSERT INTO users(username, name, email, password, sessionID, connectionID) VALUES ('maria12','Maria','maria@gmail.com','$1$AcmtC.1E$oYZDUYG7RYsHefXEJNUkq/','0',0);