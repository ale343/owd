<?php
    include 'core/init.php';

    if($userObj -> isLoggedIn()){
        $userObj->redirect('home.php');
    }

    if ($_SERVER['REQUEST_METHOD'] === "POST"){
        if(isset($_POST)){
            $email = trim(stripcslashes(htmlentities($_POST['email'])));
            $password = $_POST['password'];

            if(!empty($email) && !empty($password)){
                // Validar
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $error = "Invalid Email Format";
                }else{
                    if($user = $userObj -> emailExist($email)){
                        if(password_verify($password, $user->password)){
                            session_regenerate_id();
                            $_SESSION['userID'] = $user->userID;
                            $userObj -> redirect('home.php');
                        }else{
                            $error = "Incorrect mail or password";
                        }
                    }
                }
            }else{
                $error = "Enter your email and password to login";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to OWD</title>
    <link rel = "icon" href = "assets/images/connect.ico" type = "image/x-icon">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="assets/css/style.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <div class="sidenav">
        <div class="login-main-text">
           <h1>OWD</h1>
           <h3>Log in to talk to people all around the world</h3>

           <p>A new way to connect safety with anyone</p>
        </div>
   </div>
   <div class="main">
        <div class="col-md-6 col-sm-12">
            <div class="login-form">
                <form method = "post">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-black">Login</button>
                    <div class="error-style">
                        <?php
                            if(isset($error)){
                                echo $error;
                            }
                        ?>
                    </div>
                </form>
                <a href="register.php"><button type="submit" class="btn btn-secondary">Register</button></a>

            </div>
        </div>
   </div>
</body>
</html>
    