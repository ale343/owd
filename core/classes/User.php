<?php
    namespace MyApp;
    use PDO;
    class User{
        public $db, $userID, $sessionID;

        public function __construct(){
            $db  = new \MyApp\DB;
            $this -> db = $db -> connect();
            $this -> userID = $this -> ID();
            $this -> sessionID = $this -> getSessionID();
        }

        public function ID(){
            if ($this->isLoggedIn()){
                return $_SESSION['userID'];
            }
        }

        public function getSessionID(){
            return session_id();
        }

        public function emailExist($email){
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `email` = :email");
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->execute();
            $user = $query->fetch(PDO::FETCH_OBJ);

            if(!empty($user)){
                return $user;
            }else{
                return false;
            }
        }

        public function usernameExist($username){
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `username` = :username");
            $query->bindParam(":username", $username, PDO::PARAM_STR);
            $query->execute();
            $user = $query->fetch(PDO::FETCH_OBJ);

            if(!empty($user)){
                return $user;
            }else{
                return false;
            }
        }

        public function hash($password){
            return password_hash($password, PASSWORD_DEFAULT);
        }

        public function redirect($location){
            header("Location: ".BASE_URL.$location);
        }

        public function userData($userID = ''){
            $userID = ((!empty($userID)) ? $userID : $this->userID);
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `userID` = :userID");
            $query->bindParam(":userID", $userID, PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(PDO::FETCH_OBJ);            
        }

        public function isLoggedIn(){
            return ((isset($_SESSION['userID'])) ? true : false);
        }

        public function registerUser($email, $password, $username, $name){
            $profileImage = 'assets/images/user.png';
            $sessionID = '0';
            $connectionID = 0;
            $password = $this->hash($password);
            $query = $this->db->prepare("INSERT INTO users (username,name,email,password,profileImage,sessionID,connectionID) VALUES (:username, :name, :email, :password, :profileImage, :sessionID, :connectionID);");
            $query->bindParam(":username", $username, PDO::PARAM_STR);
            $query->bindParam(":name", $name, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":password", $password, PDO::PARAM_STR);
            $query->bindParam(":profileImage", $profileImage, PDO::PARAM_STR);
            $query->bindParam(":sessionID", $sessionID, PDO::PARAM_STR);
            $query->bindParam(":connectionID", $connectionID, PDO::PARAM_STR);

            if($query->execute()){
                echo '<script>console.log("Good stuff here")</script>';

            }else{
                echo '<script>console.log("Bad stuff here")</script>';

            }
        }

        public function logout(){
            $_SESSION =  array();
            session_destroy();
            session_regenerate_id();
            $this->redirect('index.php');
        }

        public function getUsers(){
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `userID` != :userID");
            $query->bindParam(":userID", $this->userID, PDO::PARAM_STR);
            $query->execute();
            $users = $query->fetchAll(PDO::FETCH_OBJ); 
            foreach ($users as $user){
                echo '<li class="select-none transition hover:bg-green-50 p-4 cursor-pointer select-none">
                        <a href="'.BASE_URL."connect.php?username=".$user->username.'">
                            <div class="user-box flex items-center flex-wrap">
                            <div class="flex-shrink-0 user-img w-14 h-14 rounded-full border overflow-hidden">
                                <img class="w-full h-full" src="'.BASE_URL.$user->profileImage.'">
                            </div>
                            <div class="user-name ml-2">
                                <div><span class="flex font-medium">'.$user->name.'</span></div>
                                <div></div>
                            </div>
                            </div>
                        </a>
                    </li>';
            }
        }

        public function getUserByUsername($username){
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `username` = :username");
            $query->bindParam(":username", $username, PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(PDO::FETCH_OBJ);            
        }

        public function updateSession(){
            $query = $this->db->prepare("UPDATE `users` SET `sessionID` = :sessionID WHERE `userID` = :userID");
            $query->bindParam(":sessionID", $this->sessionID, PDO::PARAM_STR);
            $query->bindParam(":userID", $this->userID, PDO::PARAM_INT);
            $query->execute();
        }

        public function getUserBySession($sessionID){
            $query = $this->db->prepare("SELECT * FROM `users` WHERE `sessionID` = :sessionID");
            $query->bindParam(":sessionID", $sessionID, PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(PDO::FETCH_OBJ);
        } 

        public function updateConnection($connectionID, $userID){
			$query = $this->db->prepare("UPDATE `users` SET `connectionID` = :connectionID WHERE `userID` = :userID");
			$query->bindParam(":connectionID", $connectionID, PDO::PARAM_STR);
			$query->bindParam(":userID", $userID, PDO::PARAM_INT);
			$query->execute();
		}
    }