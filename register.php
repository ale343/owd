<?php
    include 'core/init.php';

    if($userObj -> isLoggedIn()){
        $userObj->redirect('home.php');
    }

    if ($_SERVER['REQUEST_METHOD'] === "POST"){
        if(isset($_POST)){
            $email = trim(stripcslashes(htmlentities($_POST['email'])));
            $password = $_POST['password'];
            $username = $_POST['user'];
            $name = $_POST['name'];


            if(!empty($email) && !empty($password) && !empty($username) && !empty($name)){
                // Validar
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $error = "Invalid Email Format";
                }else{
                    if($userObj -> emailExist($email)){
                        $error = "Email already exists";
                    }elseif($userObj -> usernameExist($username)){
                        $error = "Username already exists";
                    }else{
                        $userObj -> registerUser($email, $password, $username, $name);
                        session_regenerate_id();
                        $_SESSION['userID'] = $user->userID;
                        $userObj -> redirect('home.php');
                    }
                }
            }else{
                $error = "Missing values";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel = "icon" href = "assets/images/connect.ico" type = "image/x-icon">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="assets/css/style.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <div class="sidenav">
        <div class="login-main-text">
           <h1>OWD</h1>
           <h3>Sign Up with us!</h3>
           <p>You'll never regret the amount of new experiences that you can have here.</p>
        </div>
   </div>
   <div class="main">
        <div class="col-md-6 col-sm-12">
            <div class="login-form">
                <form method = "post">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="user" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    
                    <button type="submit" class="btn btn-black">Registrar</button>
                    <div class="error-style">
                        <?php
                            if(isset($error)){
                                echo $error;
                            }
                        ?>
                    </div>
                </form>
            </div>
        </div>
   </div>
</body>
</html>
    